<?php $themeID = get_queried_object()->term_id;
		    setcookie('related-category', $themeID, time() + (86400 * 30), "/"); get_header(); ?>

<div id="main-content">
	<div class="container noline">
		<div id="content-area" class="clearfix">
		<?php
			/*$args = array(
	          'post_type'      => 'product_slider',
	          'post_status'    => 'publish',
	          'meta_query'     => array(),
	          'meta_key'       => 'product_line_category',
      		  'meta_value'     => 7,
	          // 'meta_key'       => 'product_flavor_category',
	          // 'meta_value'     => get_queried_object()->term_id,
	          'orderby'        => 'asc',
	          'posts_per_page' => '1'
		    );
		    $args['meta_query'][] = array(
		      'key' => 'product_flavor_category',
		      'value' => get_queried_object()->term_id,
		      'compare' => 'LIKE'
		    );*/
	      	$posts = new WP_Query(array('post_type' => 'recipes', 'p' => get_field('signature_recipe')));
			if ( $posts->have_posts() && get_field('signature_recipe') ) :
				while ( $posts->have_posts() ) : $posts->the_post();
					$post_format = et_pb_post_format(); 
					$flavor = get_field('product_flavor_category');
					$custom_cursor = get_field('custom_cursor_icon');
                  	$custom_class = get_term($custom_cursor, 'custom_cursors');
					$taxInfo = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
					?>
					<article id="post-<?php the_ID(); ?>" class="et_pb_post <?php echo $custom_class->slug; ?>">
						<div class="et_pb_section et_pb_section_1 et_section_regular">
							<?php
							/*$featured_recipe = get_field('signature_recipe');
							if($featured_recipe):
								$recipe = new WP_Query(array('post_type' => 'recipes', 'p' => get_field('signature_recipe')));
								while ($recipe->have_posts()) :
								    $recipe->the_post();
								    $recipeImage = get_the_post_thumbnail_url();
								    $recipeTitle = get_the_title();*/
								
							?>
								<div class="et_pb_row featured_recipe">
			                  		<div class="et_pb_column et_pb_column_1_4"><img src="<?php the_post_thumbnail_url(); ?>" /></div>
			                  		<div class="et_pb_column et_pb_column_3_4">
			                  			<h3>Featured Cocktail</h3>
			                  			<h1><?php the_title(); ?></h1>
			                  			<div class="et_pb_row et_pb_row_fullwidth recipe_details">
			                  				<div class="et_pb_column et_pb_column_1_2">
			                  					<h6>Ingredients</h6>
			                  					<?php the_field('ingredients'); ?>
			                  				</div>
			                  				<div class="et_pb_column et_pb_column_1_2">
			                  					<h6>Instructions</h6>
			                  					<?php the_field('instructions'); ?>
			                  					<br /><br />
			                  					<a class="where_to_buy" href="/where-to-buy"><i class="fas fa-map-marker-alt"></i><span>Where to buy</span></a>
			                  				</div>
			                  			</div>
			                  		</div>
			                  	</div>
			                <?php //endwhile; $posts->reset_postdata(); endif; ?>
			                
			                <h4 class="center">Related <?php echo $taxInfo->name; ?> Recipes</h4>
			                <?php echo do_shortcode('[recipe_slider theme="' . $themeID . '"]'); ?>
			            	<hr class="dots" />
			                <h3 class="center">Recipes by theme</h3>
			                <div class="et_pb_row et_pb_row_fullwidth theme grid">
			                	<?php echo do_shortcode('[show_themes]'); ?>
			                </div>
				            <hr class="dots" />
			                <h3 class="center">View recipes by your Kinky flavor</h3>
			                <div class="et_pb_row et_pb_row_fullwidth flavors grid">
			                	<?php echo do_shortcode('[show_flavors]'); ?>
			                </div>
			                <h2 class="center">Kinky Product Lines</h2>
							<div class="et_pb_row et_pb_row_fullwidth product_line grid">
			                	<?php echo do_shortcode('[show_product_lines]'); ?>
			                </div>
							
						</div>
					</article> <!-- .et_pb_post -->
			<?php
				endwhile;
			else :
				echo '<h2>A signature recipe must be assigned by the administrator</h2><p>Please choose a signature recipe for this theme to view this page.</p>';
			endif;
			?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>