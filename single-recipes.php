<?php get_header(); ?>

<div id="main-content">
	<div class="container noline">
		<div id="content-area" class="clearfix">
		<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					$post_format = et_pb_post_format();
					$flavor = get_field('recipe_flavor_category');
					$theme = get_field('recipe_theme_category');
					$custom_cursor = get_field('custom_cursor_icon');
					$custom_class = get_term($custom_cursor, 'custom_cursors');
					if(isset($_COOKIE['related-category']) && $_COOKIE['related-category'] != 'flavor') {
					    $taxInfo = get_term($_COOKIE['related-category'], 'recipe_categories_theme');
					} else {
					    $taxInfo = get_term($flavor[0], 'recipe_categories_flavor');
					};
					?>
					<article id="post-<?php the_ID(); ?>" class="et_pb_post <?php echo $custom_class->slug; ?>">
						<div class="et_pb_section et_pb_section_1 et_section_regular">
								<div class="et_pb_row featured_recipe">
			                  		<div class="et_pb_column et_pb_column_1_4"><img src="<?php the_post_thumbnail_url(); ?>" /></div>
			                  		<div class="et_pb_column et_pb_column_3_4">
			                  			<h1><?php the_title(); ?></h1>
			                  			<div class="et_pb_row et_pb_row_fullwidth recipe_details">
			                  				<div class="et_pb_column et_pb_column_1_2">
			                  					<h6>Ingredients</h6>
			                  					<?php the_field('ingredients'); ?>
			                  				</div>
			                  				<div class="et_pb_column et_pb_column_1_2">
			                  					<h6>Instructions</h6>
			                  					<?php the_field('instructions'); ?>
			                  					<br /><br />
			                  					<a class="where_to_buy" href="/where-to-buy"><i class="fas fa-map-marker-alt"></i><span>Where to buy</span></a>
			                  				</div>
			                  			</div>
			                  		</div>
			                  	</div>
			                
			                <h4 class="center">Related <?php echo $taxInfo->name; ?> Recipes</h4>
			                <?php
			                	if(isset($_COOKIE['related-category']) && $_COOKIE['related-category'] != 'flavor') {
								    echo do_shortcode('[recipe_slider theme="' . $_COOKIE['related-category'] . '"]');
								} else {
								    echo do_shortcode('[recipe_slider category="' . $flavor[0] . '"]');
								}; 
			                ?>
			            	<hr class="dots" />
			                <h3 class="center">Recipes by theme</h3>
			                <div class="et_pb_row et_pb_row_fullwidth theme grid">
			                	<?php echo do_shortcode('[show_themes]'); ?>
			                </div>
				            <hr class="dots" />
			                <h3 class="center">View recipes by your Kinky flavor</h3>
			                <div class="et_pb_row et_pb_row_fullwidth flavors grid">
			                	<?php echo do_shortcode('[show_flavors]'); ?>
			                </div>
			                <h2 class="center">Kinky Product Lines</h2>
							<div class="et_pb_row et_pb_row_fullwidth product_line grid">
			                	<?php echo do_shortcode('[show_product_lines]'); ?>
			                </div>
							
						</div>
					</article> <!-- .et_pb_post -->
			<?php
				endwhile;
			else :
				get_template_part( 'includes/no-results', 'index' );
			endif;
			?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>