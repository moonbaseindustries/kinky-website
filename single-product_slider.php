<?php get_header(); ?>

<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
		<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					$post_format = et_pb_post_format(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>
						<div class="et_pb_section et_pb_section_1 et_section_regular">
							<?php
							$featured_recipe = get_field('signature_recipe');
							if($featured_recipe):
								$post = $featured_recipe;
								setup_postdata($post);
							?>
								<div class="et_pb_row et_pb_row_fullwidth" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
			                  		<div class="et_pb_column et_pb_column_1_4">&nbsp;</div>
			                  		<div class="et_pb_column et_pb_column_3_4">
			                  			<h3>Featured Cocktail</h3>
			                  			<h1><?php the_title(); ?></h1>
			                  			<div class=" et_pb_row et_pb_row_fullwidth">
			                  				<div class="et_pb_column et_pb_column_1_2">
			                  					<h6>Ingredients</h6>
			                  					<?php the_field('ingredients'); ?>
			                  				</div>
			                  				<div class="et_pb_column et_pb_column_1_2">
			                  					<h6>Instructions</h6>
			                  					<?php the_field('instructions'); ?>
			                  					<br /><br />
			                  					<a href="">Where to buy</a>
			                  				</div>
			                  			</div>
			                  		</div>
			                  	</div>
			                <?php wp_reset_postdata(); endif; ?>
			                <h4>Related Recipes</h4>
			                <?php 
			                	$flavor = get_field('product_flavor_category');
			                	echo do_shortcode('[recipe_slider category="' . $flavor[0] . '"]'); 
			                ?>
			                <hr class="dots" />
			                <h3>Recipes by theme</h3>
			                <div class="et_pb_row et_pb_row_fullwidth recipes">
			                	<?php echo do_shortcode('[show_themes]'); ?>
			                </div>

			                <hr class="dots" />
			                <h3>View recipes by your Kinky flavor</h3>
			                <div class="et_pb_row et_pb_row_fullwidth flavors">
			                	<?php echo do_shortcode('[show_flavors]'); ?>
			                </div>

			                <h2>Kinky Product Lines</h2>
							<?php // the_content(); ?>
						</div>
					</article> <!-- .et_pb_post -->
			<?php
				endwhile;
			else :
				get_template_part( 'includes/no-results', 'index' );
			endif;
			?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>