<?php 

add_action( 'wp_enqueue_scripts', 'my_enqueue_assets' ); 

function my_enqueue_assets() { 

    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' ); 

}

function my_added_social_icons($kkoptions) {
	global $themename, $shortname;
	
	$open_social_new_tab = array( "name" =>esc_html__( "Open Social URLs in New Tab", $themename ),
                   "id" => $shortname . "_show_in_newtab",
                   "type" => "checkbox",
                   "std" => "off",
                   "desc" =>esc_html__( "Set to ON to have social URLs open in new tab. ", $themename ) );
				   
	$replace_array_newtab = array ( $open_social_new_tab );
	
	$show_instagram_icon = array( "name" =>esc_html__( "Show Instagram Icon", $themename ),
                   "id" => $shortname . "_show_instagram_icon",
                   "type" => "checkbox2",
                   "std" => "on",
                   "desc" =>esc_html__( "Here you can choose to display the Instagram Icon on your header or footer. ", $themename ) );
	$show_pinterest_icon = array( "name" =>esc_html__( "Show Pinterest Icon", $themename ),
                   "id" => $shortname . "_show_pinterest_icon",
                   "type" => "checkbox2",
                   "std" => "on",
                   "desc" =>esc_html__( "Here you can choose to display the Pinterest Icon on your header or footer. ", $themename ) );
	$show_tumblr_icon = array( "name" =>esc_html__( "Show Tumblr Icon", $themename ),
                   "id" => $shortname . "_show_tumblr_icon",
                   "type" => "checkbox2",
                   "std" => "on",
                   "desc" =>esc_html__( "Here you can choose to display the Tumblr Icon on your header or footer. ", $themename ) );
	$show_dribbble_icon = array( "name" =>esc_html__( "Show Dribbble Icon", $themename ),
                   "id" => $shortname . "_show_dribbble_icon",
                   "type" => "checkbox2",
                   "std" => "on",
                   "desc" =>esc_html__( "Here you can choose to display the Dribbble Icon on your header or footer. ", $themename ) );
	$show_vimeo_icon = array( "name" =>esc_html__( "Show Vimeo Icon", $themename ),
                   "id" => $shortname . "_show_vimeo_icon",
                   "type" => "checkbox2",
                   "std" => "on",
                   "desc" =>esc_html__( "Here you can choose to display the Vimeo Icon on your header or footer. ", $themename ) );
	$show_linkedin_icon = array( "name" =>esc_html__( "Show LinkedIn Icon", $themename ),
                   "id" => $shortname . "_show_linkedin_icon",
                   "type" => "checkbox2",
                   "std" => "on",
                   "desc" =>esc_html__( "Here you can choose to display the LinkedIn Icon on your header or footer. ", $themename ) );
	$show_myspace_icon = array( "name" =>esc_html__( "Show MySpace Icon", $themename ),
                   "id" => $shortname . "_show_myspace_icon",
                   "type" => "checkbox2",
                   "std" => "on",
                   "desc" =>esc_html__( "Here you can choose to display the MySpace Icon on your header or footer. ", $themename ) );
	$show_skype_icon = array( "name" =>esc_html__( "Show Skype Icon", $themename ),
                   "id" => $shortname . "_show_skype_icon",
                   "type" => "checkbox2",
                   "std" => "on",
                   "desc" =>esc_html__( "Here you can choose to display the Skype Icon on your header or footer. ", $themename ) );
	$show_youtube_icon = array( "name" =>esc_html__( "Show Youtube Icon", $themename ),
                   "id" => $shortname . "_show_youtube_icon",
                   "type" => "checkbox2",
                   "std" => "on",
                   "desc" =>esc_html__( "Here you can choose to display the Youtube Icon on your header or footer. ", $themename ) );
	$show_flickr_icon = array( "name" =>esc_html__( "Show Flickr Icon", $themename ),
                   "id" => $shortname . "_show_flickr_icon",
                   "type" => "checkbox2",
                   "std" => "on",
                   "desc" =>esc_html__( "Here you can choose to display the Flickr Icon on your header or footer. ", $themename ) );
				   
	$repl_array_opt = array( $show_instagram_icon,
							$show_pinterest_icon,
							$show_tumblr_icon,
							$show_dribbble_icon,
							$show_vimeo_icon,
							$show_linkedin_icon,
							$show_myspace_icon,
							$show_skype_icon,
							$show_youtube_icon,
							$show_flickr_icon,
							);
	
	$show_instagram_url =array( "name" =>esc_html__( "Instagram Profile Url", $themename ),
                   "id" => $shortname . "_instagram_url",
                   "std" => "#",
                   "type" => "text",
                   "validation_type" => "url",
				   "desc" =>esc_html__( "Enter the URL of your Instagram Profile. ", $themename ) );
	$show_pinterest_url =array( "name" =>esc_html__( "Pinterest Profile Url", $themename ),
                   "id" => $shortname . "_pinterest_url",
                   "std" => "#",
                   "type" => "text",
                   "validation_type" => "url",
				   "desc" =>esc_html__( "Enter the URL of your Pinterest Profile. ", $themename ) );
	$show_tumblr_url =array( "name" =>esc_html__( "Tumblr Profile Url", $themename ),
                   "id" => $shortname . "_tumblr_url",
                   "std" => "#",
                   "type" => "text",
                   "validation_type" => "url",
				   "desc" =>esc_html__( "Enter the URL of your Tumblr Profile. ", $themename ) );
	$show_dribble_url =array( "name" =>esc_html__( "Dribbble Profile Url", $themename ),
                   "id" => $shortname . "_dribble_url",
                   "std" => "#",
                   "type" => "text",
                   "validation_type" => "url",
				   "desc" =>esc_html__( "Enter the URL of your Dribbble Profile. ", $themename ) );
	$show_vimeo_url =array( "name" =>esc_html__( "Vimeo Profile Url", $themename ),
                   "id" => $shortname . "_vimeo_url",
                   "std" => "#",
                   "type" => "text",
                   "validation_type" => "url",
				   "desc" =>esc_html__( "Enter the URL of your Vimeo Profile. ", $themename ) );
	$show_linkedin_url =array( "name" =>esc_html__( "LinkedIn Profile Url", $themename ),
                   "id" => $shortname . "_linkedin_url",
                   "std" => "#",
                   "type" => "text",
                   "validation_type" => "url",
				   "desc" =>esc_html__( "Enter the URL of your LinkedIn Profile. ", $themename ) );
	$show_myspace_url =array( "name" =>esc_html__( "MySpace Profile Url", $themename ),
                   "id" => $shortname . "_mysapce_url",
                   "std" => "#",
                   "type" => "text",
                   "validation_type" => "url",
				   "desc" =>esc_html__( "Enter the URL of your MySpace Profile. ", $themename ) );
	$show_skype_url =array( "name" =>esc_html__( "Skype Profile Url", $themename ),
                   "id" => $shortname . "_skype_url",
                   "std" => "#",
                   "type" => "text",
                   "validation_type" => "url",
				   "desc" =>esc_html__( "Enter the URL of your Skype Profile. ", $themename ) );
	$show_youtube_url =array( "name" =>esc_html__( "Youtube Profile Url", $themename ),
                   "id" => $shortname . "_youtube_url",
                   "std" => "#",
                   "type" => "text",
                   "validation_type" => "url",
				   "desc" =>esc_html__( "Enter the URL of your Youtube Profile. ", $themename ) );
	$show_flickr_url =array( "name" =>esc_html__( "Flickr Profile Url", $themename ),
                   "id" => $shortname . "_flickr_url",
                   "std" => "#",
                   "type" => "text",
                   "validation_type" => "url",
				   "desc" =>esc_html__( "Enter the URL of your Flickr Profile. ", $themename ) );
				   
	$repl_array_url = array( $show_instagram_url,
							$show_pinterest_url,
							$show_tumblr_url,
							$show_dribble_url,
							$show_vimeo_url,
							$show_linkedin_url,
							$show_myspace_url,
							$show_skype_url,
							$show_youtube_url,
							$show_flickr_url,
							);


	$srch_key = array_column($kkoptions, 'id');
	
	$key = array_search('divi_show_facebook_icon', $srch_key);
	array_splice($kkoptions, $key + 6, 0, $replace_array_newtab);
	
	$key = array_search('divi_show_google_icon', $srch_key);
	array_splice($kkoptions, $key + 8, 0, $repl_array_opt);

	$key = array_search('divi_rss_url', $srch_key);
	array_splice($kkoptions, $key + 17, 0, $repl_array_url);
	
	//print_r($kkoptions);

	return $kkoptions;
}
add_filter('et_epanel_layout_data', 'my_added_social_icons', 99);

function product_slider( $atts ){
      $args = array(
          'post_type'      => 'product_slider',
          'post_status'    => 'publish',
          'meta_key'       => 'product_line_category',
          'meta_value'     => $atts['category'],
          'orderby'        => 'asc',
          'posts_per_page' => '-1'
      );
      $posts = new WP_Query($args);
      if($posts->have_posts()) : 
            $term = get_term($atts['category']);
            $output = '<div id="product-slider" class="slick-slider"><div class="product-slider__container"><div class="js-product-slider ' . $term->slug . '">';
            while($posts->have_posts()) : 
                  $posts->the_post();
                  $title = get_the_title();
                  $category_id = get_field('product_flavor_category');
                  $also_like = get_field('related_products');
                  $flavor = get_field('product_flavor_category');
                  $custom_cursor = get_field('custom_cursor_icon');
                  $custom_class = get_term($custom_cursor, 'custom_cursors');
                  $taxInfo = get_term($flavor[0], 'recipe_categories_flavor');
                  $recipe = new WP_Query(array('post_type' => 'recipes', 'p' => get_field('signature_recipe')));
                  while ($recipe->have_posts()) :
                        $recipe->the_post();
                        $recipeImage = get_the_post_thumbnail_url();
                        $recipeTitle = get_the_title();
                        $recipeLink = get_the_permalink();
                  endwhile;
                  $posts->reset_postdata();
                  $output .= '<div class="slide ' . $custom_class->slug . '">' .
                      '<img src="' . get_the_post_thumbnail_url() . '" />' .
                      '<img src="' . get_field('product_highlight') . '" />' .
                      '<div class="info hidden"><div class="desc">' .
                          '<span class="title ' . $taxInfo->slug . '">' . $title . '</span>' .
                          //'<p>' . get_field('flavor_profile') . '</p>' .
                          '<a class="small-button smallblue" href="">Details <i class="fas fa-chevron-down"></i><i class="fas fa-chevron-up"></i></a></div>' .
                          '<div class="details"><div class="top">' .
                              '<p>' . get_field('flavor_profile') . '</p>' .
                              '<a class="where_to_buy" href="/where-to-buy"><i class="fas fa-map-marker-alt"></i><span>Where to buy</span></a>';
                              if($atts['category'] == 7){
                              $output .= '<div class="recipe">' .
                                    '<div class="left"><img src="' . $recipeImage . '" /></div>' .
                                    '<div class="right">Signature Drink<h4>' . $recipeTitle . '</h4><a class="small-button smallblue" href="' . $recipeLink . '">View Cocktail</a><br /><a href="' . get_term_link($category_id[0]) . '" class="view-more">View other ' . $title . ' recipes ></a></div>' .
                              '</div>';
                              }
                              $output .= '</div><div class="bottom"><hr class="dots" /><div class="also_like"><h4>You might also like</h4>';
                              foreach($also_like as $also){
                                    $output .= '<img src="' . $also[related_product_image][url] . '" />';
                              }
                        $output .= '</div></div></div></div>' .
                  '</div>';
            endwhile;
            $output .= '</div></div><div class="ajax-details"></div></div><!-- end of product slider -->';

            return $output;
      endif;

      // $args['meta_query'][] = array(
      //       'key' => $key,
      //       'value' => $value,
      //       'compare' => 'LIKE'
      // );
      // $output = '<script src="'. get_stylesheet_directory_uri() .'/main.js"></script>';


      // return $output;
}
add_shortcode( 'product_slider', 'product_slider' );

function recipe_slider( $atts ){
      $filter = 'recipe_flavor_category';
      if($atts['theme'] && $atts['theme'] != ''){
            $filter = 'recipe_theme_category';
            $target = $atts['theme'];
      }else if($atts['category'] && $atts['category'] != ''){
            $target = $atts['category'];
      }
      // $flavor = get_term_by('slug', 'pink', 'recipe_categories_flavor');
      // print_r($flavor->term_id);
      $term = get_term($target);
      $args = array(
          'post_type'      => 'recipes',
          'post_status'    => 'publish',
          'meta_query'     => array(),
          'orderby'        => 'asc',
          'posts_per_page' => '-1'
      );
      $args['meta_query'][] = array(
            'key' => $filter,
            'value' => $target,
            'compare' => 'LIKE'
      );
      $posts = new WP_Query($args);
      if($posts->have_posts()) :
            $output = '<div id="recipe-slider" class="slick-slider"><div class="recipe-slider__container"><div class="js-recipe-slider">';
            while($posts->have_posts()) : 
                  $posts->the_post();
                  // $filterID = get_field($filter);
                  // foreach($filterID as $id){
                        // if($id == $target){
                              $output .= '<div style="background-image:url(' . get_the_post_thumbnail_url() . ')"><a class="' . $term->slug . '" href="' . get_the_permalink() . '"><span>' . get_the_title() . '</span></a></div>';
                        // };
                  // }
            endwhile;
            $output .= '</div></div></div><!-- end of recipe slider -->';
            return $output;
      endif;
}
add_shortcode( 'recipe_slider', 'recipe_slider' );

function show_themes(){
      $args = array('orderby'=>'asc','hide_empty'=>false);
      $custom_terms = get_terms('recipe_categories_theme', $args);
      $output = '';
      foreach($custom_terms as $term){
            $myID = $term->taxonomy . '_' . $term->term_id;
            $custom_cursor = get_field('cursor_icon', $myID);
            $custom_class = get_term($custom_cursor, 'custom_cursors');
            if(get_field('featured', $myID) == 1){
                  $image_url = get_field('category_image', $myID);
                  $image_url = $image_url['url'];

                  $output .= '<div class="grid_element"><a class="' . $custom_class->slug . '" href="' . get_term_link($term->term_id) . '">';
                  $output .= '<img src="' . $image_url . '" />';
                  $output .= $term->name;
                  $output .= '</a></div>';
            }
      }
      return $output;
}
add_shortcode( 'show_themes', 'show_themes' );

function show_flavors(){
      $args = array('orderby'=>'asc','hide_empty'=>false);
      $custom_terms = get_terms('recipe_categories_flavor', $args);
      $output = '';
      foreach($custom_terms as $term){
            $myID = $term->taxonomy.'_'.$term->term_id;
            $custom_cursor = get_field('cursor_icon', $myID);
            $custom_class = get_term($custom_cursor, 'custom_cursors');
            $image_url = get_field('category_image', $myID);
            $image_url = $image_url['url'];

            $output .= '<div class="grid_element"><a class="' . $custom_class->slug . '" href="' . get_term_link($term->term_id) . '">';
            $output .= '<img src="' . $image_url . '" />';
            $output .= $term->name;
            $output .= '</a></div>';
      }
      return $output;
}
add_shortcode( 'show_flavors', 'show_flavors' );

function show_product_lines(){
      $args = array(
          'post_type'      => 'product_line_blocks',
          'post_status'    => 'publish',
          'orderby'        => 'asc',
          'posts_per_page' => '3'
      );
      $posts = new WP_Query($args);
      if($posts->have_posts()) :
            $output = '';
            while($posts->have_posts()) : 
                  $posts->the_post();
                  $output .= '<div class="grid_element"><a href="' . get_the_permalink() . '">';
                  $output .= '<img src="' . get_the_post_thumbnail_url() . '" />';
                  $output .= '</a></div>';
            endwhile;
            return $output;
      endif;
}
add_shortcode( 'show_product_lines', 'show_product_lines' );

function add_flavor_styles(){
      $styles = get_terms(array('taxonomy'=>'custom_cursors', 'hide_empty'=>false));
      $output = '<style type="text/css">';
      foreach($styles as $style){
            $myID = $style->taxonomy . '_' . $style->term_id;
            $cursor = get_field('cursor_icon', $myID);
            $color = get_field('custom_color', $myID);
            // print_r($style);
            $output .= '.js-product-slider .slide.' . $style->slug . ' img:hover{cursor : url(' . $cursor . ') 15 15, auto;}' . PHP_EOL;
            $output .= '.title.' . $style->slug . '{color :' . $color . ';}' . PHP_EOL;
            $output .= '.' . $style->slug . ' .featured_recipe h1{color :' . $color . ';}' . PHP_EOL;
            $output .= 'a.' . $style->slug . ':hover{cursor : url(' . $cursor . ') 15 15, auto;}' . PHP_EOL;
      };
      $output .= '</style>';
      return $output;
}
add_shortcode( 'add_flavor_styles', 'add_flavor_styles' );

define( 'DDPL_DOMAIN', 'my-domain' ); // translation domain
require_once( 'vendor/divi-disable-premade-layouts/divi-disable-premade-layouts.php' );

?>