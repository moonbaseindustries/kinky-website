(function($) {
	$(document).ready(() => {
		console.log('document ready', $('.js-product-slider'));
		if($('#product-slider').length > 0){
			var maxSlides = 5;
			var numSlides = $('.js-product-slider').find('.slide').length;
			if(numSlides <= 5){
				maxSlides = 3;
			}
			if(numSlides <= 3){
				maxSlides = 1;
			}
			$('#product-slider').addClass('autowidth-' + maxSlides);
			_initProductSlider(false, maxSlides);
		};
		if($('.js-recipe-slider').length > 0){
			_initRecipeSlider(false);
		};
	});


	function _initProductSlider(isResize, maxSlides) {
		var els = {
	      window: $(window),
	      productSlider: $('.js-product-slider'),
	      details: $('.ajax-details')
	    };
		try {
		  if (isResize) {
		    els.productSlider.slick('unslick');
		  }
		} catch(e) {
		  console.log('e', e);
		}

		els.productSlider.on('init', function(slick){
		  var markup = els.productSlider.find('.slick-center').find('.info').html();
		  els.details.html(markup);
		  $('.ajax-details').find('.desc').find('a').unbind().click(function(e){
			e.preventDefault();
			if($(this).hasClass('open')){
				$(this).removeClass('open');
			}else{
				$(this).addClass('open');
			}
			els.details.find('.details').slideToggle();
		  });
		});

		// setTimeout(_reHeight, 500);
		console.log('els.productSlider', els.productSlider);
		els.productSlider.slick({
		  centerMode: true,
		  centerPadding: '0',
		  slidesToShow: maxSlides,
		  arrows: true,
		  swipe: true,
		  infinite: true,
		  dots: false,
		  prevArrow: '<button type="button" class="slick-prev fa fa-chevron-circle-left"></button>',
		  nextArrow: '<button type="button" class="slick-next fa fa-chevron-circle-right"></button>',
		  // cssEase: 'easeInOutCubic',
		  responsive: [
		    {
		      breakpoint: 980,
		      settings: {
		        arrows: true,
		        centerMode: true,
		        centerPadding: '0px',
		        dots: false,
		        slidesToShow: 3
		      }
		    },
		    {
		      breakpoint: 500,
		      settings: {
		        arrows: true,
		        centerMode: true,
		        centerPadding: '0px',
		        dots: false,
		        slidesToShow: 1
		      }
		    }
		  ]
		});
		els.productSlider.on('beforeChange', function(event){
			console.log('beforeChange');
			els.details.find('.details').slideUp(function(){
				els.details.fadeTo(0, function(){
					$('.ajax-details').find('.desc').find('a').removeClass('open');
					$(this).empty();
				});
			});
		});
		els.productSlider.on('afterChange', function(event){
			var markup = els.productSlider.find('.slick-center').find('.info').html();
		  	console.log('markup', markup);
		  	els.details.html(markup);
		  	els.details.fadeTo(1);
		  	$('.ajax-details').find('.desc').find('a').unbind().click(function(e){
				e.preventDefault();
				if($(this).hasClass('open')){
					$(this).removeClass('open');
				}else{
					$(this).addClass('open');
				}
				els.details.find('.details').slideToggle();
			});
		});
		/*els.productSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
		  if(els.window.width() > 500){
		    $('.slick-track').find('.slick-slide').each(function(){
		      $(this).removeClass('left');
		      $(this).removeClass('right');
		    });
		    $(slick.$slides[nextSlide]).prev().addClass('left');
		    $(slick.$slides[nextSlide]).next().addClass('right');
		  };
		});*/
	}
	function _initRecipeSlider(isResize) {
		var els = {
	      window: $(window),
	      recipeSlider: $('.js-recipe-slider')
	    };
		try {
		  if (isResize) {
		    els.recipeSlider.slick('unslick');
		  }
		} catch(e) {
		  console.log('e', e);
		}
		console.log('els.recipeSlider', els.recipeSlider);
		els.recipeSlider.slick({
		  centerPadding: '0',
		  slidesToShow: 5,
		  slidesToScroll: 1,
		  arrows: true,
		  swipe: true,
		  infinite: true,
		  dots: false,
		  prevArrow: '<button type="button" class="slick-prev fa fa-chevron-circle-left"></button>',
		  nextArrow: '<button type="button" class="slick-next fa fa-chevron-circle-right"></button>',
		  responsive: [
		    {
		      breakpoint: 980,
		      settings: {
		        arrows: true,
		        centerPadding: '0px',
		        dots: false,
		        slidesToShow: 1
		      }
		    }
		  ]
		});
	}
})(jQuery);